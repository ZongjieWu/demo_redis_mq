package com.wzj.demo_redis_mq;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;

/**
 * @author Zongjie Wu
 * @date 2021/3/11 15:20
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoRedisMqApplication.class)
public class MQConsumerTest {
    @Autowired
    private StringRedisTemplate redisTemplate;
    //redis的消息队列直接使用redis数组实现
    private ListOperations<String, String> listRedis;

    @PostConstruct
    private void init(){
        listRedis = redisTemplate.opsForList();
    }

//    @Test
//    public void test() {
//        while(true){
//            //从右边取堆栈顺序取1~10个消息
//            String msg = listRedis.rightPop("storage");
//            if(StringUtils.isEmpty(msg)){
//                System.out.println("消息已经全部取出了。。。。");
//                break;
//            }
//            System.out.println(msg);
//        }
//    }
}
