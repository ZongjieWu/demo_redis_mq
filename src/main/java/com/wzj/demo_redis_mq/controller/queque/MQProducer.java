package com.wzj.demo_redis_mq.controller.queque;

import com.alibaba.fastjson.JSON;
import com.wzj.demo_redis_mq.constant.RedisKeys;
import com.wzj.demo_redis_mq.constant.SessionInfo;
import com.wzj.demo_redis_mq.dto.Message;
import com.wzj.demo_redis_mq.enums.MessageTypeEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Zongjie Wu
 * @date 2021/3/11 15:18
 */
@Slf4j
@RequestMapping("/producer")
@RestController
public class MQProducer {

    @Autowired
    private StringRedisTemplate redisTemplate;

    //redis的消息队列直接使用redis数组实现
    private ListOperations<String, String> listRedis;

    @PostConstruct
    private void init(){
        listRedis = redisTemplate.opsForList();
    }

    /**
     * 发送消息
     * @param orderNo 订单编号
     * @return
     */
    @RequestMapping("/send")
    public String test(String orderNo) {
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            //添加未支付订单，组装消息格式
            Message<String> message=new Message<>();
            message.setType(MessageTypeEnum.ORDER_UN_PAY_NO.getType());
            message.setData(orderNo);
            message.setTimestamp(System.currentTimeMillis()+ SessionInfo.ORDER_TIME_OUT_MILL_SECOND);
            listRedis.leftPush(RedisKeys.ORDER_UN_PAY_NO, JSON.toJSONString(message));
        }catch (Exception ex){
            ex.printStackTrace();
        }
        log.info("发送成功！！orderNo="+orderNo+" time="+sdf.format(new Date()));
        return "发送成功！！";
    }
}
