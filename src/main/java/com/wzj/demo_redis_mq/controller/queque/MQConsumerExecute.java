package com.wzj.demo_redis_mq.controller.queque;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.wzj.demo_redis_mq.constant.RedisKeys;
import com.wzj.demo_redis_mq.constant.SessionInfo;
import com.wzj.demo_redis_mq.dto.Message;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Zongjie Wu
 * @date 2021/3/11 16:20
 */
@Slf4j
@Component
public class MQConsumerExecute {
    @Autowired
    private StringRedisTemplate redisTemplate;
    //redis的消息队列直接使用redis数组实现
    private ListOperations<String, String> listRedis;


    @PostConstruct
    private void init(){
        listRedis = redisTemplate.opsForList();
    }

    /**
     * 一秒轮询一个
     */
    @Scheduled(fixedDelay = 1000)
    public void test() {
        //取出第一条数据
        String locl="lock";
        synchronized (locl){
            Long length=listRedis.size(RedisKeys.ORDER_UN_PAY_NO);
            if(length>0){
                String msg = listRedis.index(RedisKeys.ORDER_UN_PAY_NO,length-1);
                //反序列化
                Message<String> message = JSON.parseObject(msg,Message.class);
                //现在的时间戳
                Long nowStamp=System.currentTimeMillis();
                if(message.getTimestamp()<nowStamp){
                    //超时订单业务
                    //设置订单状态为已去消息
                    //弹出第一条数据
                    SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    listRedis.rightPop(RedisKeys.ORDER_UN_PAY_NO);
                    log.info("订单编号为"+message.getData()+"的消息已经删除 time="+sdf.format(new Date()));
                }
            }

        }

    }
}
