package com.wzj.demo_redis_mq.enums;

import lombok.Data;

/**
 * 消息类型的枚举类
 * @author Zongjie Wu
 * @date 2021/3/11 16:39
 */
public enum MessageTypeEnum {

    ORDER_UN_PAY_NO(1,"未支付订单");

    /**
     * 消息类型
     */
    private Integer type;

    /**
     * 描述
     */
    private String desc;

    MessageTypeEnum(int type,String desc){
        this.type=type;
        this.desc=desc;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
