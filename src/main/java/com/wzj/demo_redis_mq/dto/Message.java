package com.wzj.demo_redis_mq.dto;

import lombok.Data;

/**
 * 消息队列消息格式
 * @author Zongjie Wu
 * @date 2021/3/11 16:28
 */
@Data
public class Message<T> {
    /**
     * 消息类型
     */
    private Integer type;

    /**
     * 数据
     */
    private T data;

    /**
     * 时间戳(13位)
     */
    private Long timestamp;
}
