package com.wzj.demo_redis_mq.constant;

/**
 * redis key
 * @author Zongjie Wu
 * @date 2021/3/11 16:35
 */
public class RedisKeys {
    /**
     *未支付订单key
     */
    public static final String  ORDER_UN_PAY_NO="ORDER_UN_PAY_NO_";
}
