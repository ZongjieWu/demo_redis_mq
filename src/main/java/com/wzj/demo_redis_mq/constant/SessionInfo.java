package com.wzj.demo_redis_mq.constant;

/**
 * 会话的信息
 * @author Zongjie Wu
 * @date 2021/3/11 16:53
 */
public class SessionInfo {
    /**
     * 订单超时毫秒，用1分钟模拟
     */
    public static final Long ORDER_TIME_OUT_MILL_SECOND=60000L;
}
