package com.wzj.demo_redis_mq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@ComponentScan(basePackages = {"com.wzj.demo_redis_mq"})
@SpringBootApplication
public class DemoRedisMqApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoRedisMqApplication.class, args);
	}

}
